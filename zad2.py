def is_prime(num1):
    if num1 <= 1:
        return False

    for num2 in range(2, num1):
        if (num1 % num2) == 0:
            return False
    return True


def print_smaller_primes(num1):
    for num2 in range(2, num1):
        if is_prime(num2):
            print(num2, end=" ")
