a = {'Adam': 170, 'Mariusz': 185, 'Anna': 174, 'Alicja': 152}


def height(a):
    b = dict(sorted(a.items(), key=lambda item: item[1], reverse=True))
    return [*b]


print(height(a))
