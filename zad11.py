def Fibonacci(n):
    if n <= 2:
        return n - 1
    else:
        return Fibonacci(n - 1) + Fibonacci(n - 2)


print(Fibonacci(10))
