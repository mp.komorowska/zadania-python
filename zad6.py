#1
Polska = {'Nazwa': 'Polska', 'Pow': 312696, 'Zal': 38200000, 'GZal': 112, 'PKB': 17318}
Niemcy = {'Nazwa': 'Niemcy', 'Pow': 357578, 'Zal': 83019200, 'GZal': 232, 'PKB': 50842}
Czechy = {'Nazwa': 'Czechy', 'Pow': 78868, 'Zal': 10649800, 'GZal': 134, 'PKB': 20152}
Szwecja = {'Nazwa': 'Szwecja', 'Pow': 450295, 'Zal': 10327789, 'GZal': 24, 'PKB': 53867}

kraje = [Polska, Niemcy, Czechy, Szwecja]


def max_pow(kraje):
    newlist = sorted(kraje, key=lambda d: d['Pow'])
    print([d['Nazwa'] for d in newlist])


max_pow(kraje)


def max_zal(kraje):
    newlist = sorted(kraje, key=lambda d: d['Zal'])
    print([d['Nazwa'] for d in newlist])


max_zal(kraje)


def max_gzal(kraje):
    newlist = sorted(kraje, key=lambda d: d['GZal'])
    print([d['Nazwa'] for d in newlist])


max_gzal(kraje)


def max_pkb(kraje):
    newlist = sorted(kraje, key=lambda d: d['PKB'])
    print([d['Nazwa'] for d in newlist])


max_pkb(kraje)


#2
def get_max(list, key):
    return sorted(list, key=lambda d: d[key])


kraje = [
    {'Nazwa': 'Polska', 'Pow': 312696, 'Zal': 38200000, 'GZal': 112, 'PKB': 17318},
    {'Nazwa': 'Niemcy', 'Pow': 357578, 'Zal': 83019200, 'GZal': 232, 'PKB': 50842},
    {'Nazwa': 'Czechy', 'Pow': 78868, 'Zal': 10649800, 'GZal': 134, 'PKB': 20152},
    {'Nazwa': 'Szwecja', 'Pow': 450295, 'Zal': 10327789, 'GZal': 24, 'PKB': 53867},
]

sorted_list = get_max(kraje, 'Pow')
print([d['Nazwa'] for d in sorted_list])
sorted_list = get_max(kraje, 'Zal')
print([d['Nazwa'] for d in sorted_list])
sorted_list = get_max(kraje, 'PKB')
print([d['Nazwa'] for d in sorted_list])
sorted_list = get_max(kraje, 'GZal')
print([d['Nazwa'] for d in sorted_list])
