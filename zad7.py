#1
def policz(x):
    all_digits = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
    all_letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                   'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    total_digits = 0
    total_letters = 0
    for i in x:
        if i in all_digits:
            total_digits += 1
        elif i in all_letters:
            total_letters += 1
    print("Total letters found :-", total_letters)
    print("Total digits found :-", total_digits)


#2
import re


def policz_regex(word):
    digits = re.findall(r'\d', word)
    letters = re.findall(r'[A-Za-z]', word)

    print("Total letters found :-", len(letters))
    print("Total digits found :-", len(digits))
