class Pamietnik():
    def __init__(self, file):
        self.file = file

    def odczyt(self):
        with open(self.file, 'r') as file:
            return file.read()

    def dopisz(self, new_text):
        with open(self.file, 'a') as file:
            file.write(f'{new_text}\n')
          

my_diary = Pamietnik(r'') #wpisać ścieżkę
print(my_diary.odczyt())
my_diary.dopisz('')
print(my_diary.odczyt())
