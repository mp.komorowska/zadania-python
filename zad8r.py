class Kot(Zwierze):
    def __init__(self, imie):
        Zwierze.__init__(self, imie)
        self.type = type(self)

    def daj_glos(self):
        return f'Cześć, daję głos, mam na imię '


class Pies(Zwierze):
    def __init__(self, imie):
        Zwierze.__init__(self, imie)
        self.type = type(self)

    def daj_glos(self):
        return f'Cześć, daję głos, mam na imię '



kot = Kot('Kropka')
kot.powitanie()

pies = Pies('Azor')
pies.powitanie()
