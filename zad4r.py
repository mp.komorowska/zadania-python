import abc


class Figura:
    @abc.abstractmethod
    def obwod(self):
        pass
    @abc.abstractmethod
    def pole(self):
        pass


class Trojkat(Figura):
    def __init__(self, a, b, c, h):
        self.a = a
        self.b = b
        self.c = c
        self.h = h

    def obwod(self):
        return self.a + self.b + self.c

    def pole(self):
        return 0.5 * self.a * self.h


class Prostokat(Figura):
    def __init__(self, a, b):
        self.a = a
        self.b = b


    def obwod(self):
        return 2*(self.a + self.b)

    def pole(self):
        return self.a * self.b


trojkat = Trojkat(25, 56, 89, 0)
print(isinstance(trojkat, Figura))
print(trojkat.obwod())

trojkat = Trojkat(12, 0, 0, 24)
print(isinstance(trojkat, Figura))
print(trojkat.pole())

prostokat = Prostokat(12, 14)
print(isinstance(prostokat, Figura))
print(prostokat.obwod())

prostokat = Prostokat(12, 14)
print(isinstance(prostokat, Figura))
print(prostokat.pole())
