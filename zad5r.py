import math


class Linia:

    def __init__(self, x1, y1, x2, y2):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2

    def dlugosc(self):
        dx = abs(self.x2 - self.x1)
        dy = abs(self.y2 - self.y1)
        return math.sqrt((self.x2 - self.x1) ** 2 + (self.y2 - self.y1) ** 2)


linia = Linia(5, 6, 8, 12)
print(round(linia.dlugosc(), 2))
