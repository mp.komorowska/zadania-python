class Wielokat:

    def __init__(self, linie):
        if not self.check_ends(linie):
            raise ValueError('nie można utworzyć wielokąta')
        self.sides = linie

    def check_ends(self, linie):
        for i in range(len(linie)-1):
            end = (linie[i].x2, linie[i].y2)
            beggining = (linie[i + 1].x1, linie[i + 1].y1)
            if end != beggining:
                return False
            return True

    def obwod(self):
        obwod = 0
        for side in self.sides:
            obwod += side.dlugosc()
        return obwod
