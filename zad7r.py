from abc import ABC, abstractmethod


class Zwierze(ABC):

    def __init__(self, imie):
        self.imie = imie
        self.type = None

    @abstractmethod
    def daj_glos(self):
        pass

    def powitanie(self):
        a = self.daj_glos()
        print(a + self.imie + str(self.type))
