def reverse(s):
    return s == s[::-1]


def is_palindrome(s):
    if reverse(s):
        print("Yes")
    else:
        print("No")
