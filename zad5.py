from datetime import date


def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def calculate_ages(birthdays):
    for birthday in birthdays:
        print(calculate_age(birthday))


birthdays = [
    date(1948, 6, 1),
    date(1998, 6, 1),
    date(1918, 6, 1)
]

calculate_ages(birthdays)
